// import React from "react";
// import Typography from "@material-ui/core/Typography";
// import { makeStyles } from "@material-ui/core/styles";

// const useStyles = makeStyles({
//   root: {
//     width: "100%",
//     maxWidth: 500
//   }
// });

// export default function Types() {
//   const classes = useStyles();

//   return (
//     <div className={classes.root}>
//       <Typography variant="h1" component="h2" gutterBottom>
//         P7 IHM
//       </Typography>
//       <Typography variant="h2" gutterBottom>
//         P7 IHM
//       </Typography>
//       <Typography variant="h3" gutterBottom>
//         P7 IHM
//       </Typography>
//       <Typography variant="h4" gutterBottom>
//         P7 IHM
//       </Typography>
//       <Typography variant="h5" gutterBottom>
//         P7 IHM
//       </Typography>
//       <Typography variant="h6" gutterBottom>
//         P7 IHM
//       </Typography>
//       <Typography variant="subtitle1" gutterBottom>
//         subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
//         Quos blanditiis tenetur
//       </Typography>
//       <Typography variant="subtitle2" gutterBottom>
//         subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
//         Quos blanditiis tenetur
//       </Typography>
//       <Typography variant="body1" gutterBottom>
//         body1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos
//         blanditiis tenetur unde suscipit, quam beatae rerum inventore
//         consectetur, neque doloribus, cupiditate numquam dignissimos laborum
//         fugiat deleniti? Eum quasi quidem quibusdam.
//       </Typography>
//       <Typography variant="body2" gutterBottom>
//         body2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos
//         blanditiis tenetur unde suscipit, quam beatae rerum inventore
//         consectetur, neque doloribus, cupiditate numquam dignissimos laborum
//         fugiat deleniti? Eum quasi quidem quibusdam.
//       </Typography>
//       <Typography variant="button" display="block" gutterBottom>
//         button text
//       </Typography>
//       <Typography variant="caption" display="block" gutterBottom>
//         caption text
//       </Typography>
//       <Typography variant="overline" display="block" gutterBottom>
//         overline text
//       </Typography>
//     </div>
//   );
// }

// import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
// import AppBar from "@material-ui/core/AppBar";
// import Toolbar from "@material-ui/core/Toolbar";
// import Typography from "@material-ui/core/Typography";
// import Button from "@material-ui/core/Button";
// import IconButton from "@material-ui/core/IconButton";
// import MenuIcon from "@material-ui/icons/Menu";

// const useStyles = makeStyles(theme => ({
//   root: {
//     flexGrow: 1
//   },
//   menuButton: {
//     marginRight: theme.spacing(2)
//   },
//   title: {
//     flexGrow: 1
//   }
// }));

// export default function ButtonAppBar() {
//   const classes = useStyles();

//   return (
//     <div className={classes.root}>
//       <AppBar position="static">
//         <Toolbar>
//           <IconButton
//             edge="start"
//             className={classes.menuButton}
//             color="inherit"
//             aria-label="menu"
//           >
//             <MenuIcon />
//           </IconButton>
//           <Typography variant="h6" className={classes.title}>
//             News
//           </Typography>
//           <Button color="inherit">Login</Button>
//         </Toolbar>
//       </AppBar>
//     </div>
//   );
// }

// import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
// import Paper from "@material-ui/core/Paper";
// import Typography from "@material-ui/core/Typography";
// import Breadcrumbs from "@material-ui/core/Breadcrumbs";
// // import Link from "@material-ui/core/Link";
// import { Link } from "react-router-dom";

// const useStyles = makeStyles(theme => ({
//   root: {
//     justifyContent: "center",
//     flexWrap: "wrap"
//   },
//   paper: {
//     padding: theme.spacing(1, 2)
//   }
// }));

// function handleClick(event) {
//   event.preventDefault();
//   console.info("You clicked a breadcrumb.");
// }

// export default function SimpleBreadcrumbs() {
//   const classes = useStyles();

//   return (
//     <div className={classes.root}>
//       <Paper elevation={0} className={classes.paper}>
//         <Breadcrumbs aria-label="breadcrumb">
//           <Link style={{ textDecoration: "none", color: "black" }} to="/">
//             Accueil
//           </Link>
//           <Link
//             style={{
//               textDecoration: "none",
//               color: "inherit"
//             }}
//             color="inherit"
//             href="/getting-started/installation/"
//             onClick={handleClick}
//           >
//             Core
//           </Link>
//           <Typography color="inherit">Breadcrumb</Typography>
//         </Breadcrumbs>
//       </Paper>
//       <br />
//       <Paper elevation={0} className={classes.paper}>
//         <Breadcrumbs aria-label="breadcrumb">
//           <Link color="inherit" href="/" onClick={handleClick}>
//             Material-UI
//           </Link>
//           <Link
//             color="inherit"
//             href="/getting-started/installation/"
//             onClick={handleClick}
//           >
//             Core
//           </Link>
//           <Link
//             color="textPrimary"
//             href="/components/breadcrumbs/"
//             onClick={handleClick}
//             aria-current="page"
//           >
//             Breadcrumb
//           </Link>
//         </Breadcrumbs>
//       </Paper>
//     </div>
//   );
// }

/* eslint-disable no-nested-ternary */

// import React from "react";
// import PropTypes from "prop-types";
// import { makeStyles } from "@material-ui/core/styles";
// import List from "@material-ui/core/List";
// import Link from "@material-ui/core/Link";
// import ListItem from "@material-ui/core/ListItem";
// import Collapse from "@material-ui/core/Collapse";
// import ListItemText from "@material-ui/core/ListItemText";
// import Typography from "@material-ui/core/Typography";
// import ExpandLess from "@material-ui/icons/ExpandLess";
// import ExpandMore from "@material-ui/icons/ExpandMore";
// import Breadcrumbs from "@material-ui/core/Breadcrumbs";
// import { Route, MemoryRouter } from "react-router";
// import { Link as RouterLink } from "react-router-dom";

// const breadcrumbNameMap = {
//   "/inbox": "Inbox",
//   "/inbox/important": "Important",
//   "/trash": "Trash",
//   "/spam": "Spam",
//   "/drafts": "Drafts"
// };

// function ListItemLink(props) {
//   const { to, open, ...other } = props;
//   const primary = breadcrumbNameMap[to];

//   return (
//     <li>
//       <ListItem button component={RouterLink} to={to} {...other}>
//         <ListItemText primary={primary} />
//         {open != null ? open ? <ExpandLess /> : <ExpandMore /> : null}
//       </ListItem>
//     </li>
//   );
// }

// ListItemLink.propTypes = {
//   open: PropTypes.bool,
//   to: PropTypes.string.isRequired
// };

// const useStyles = makeStyles(theme => ({
//   root: {
//     display: "flex",
//     flexDirection: "column",
//     width: 360
//   },
//   lists: {
//     backgroundColor: theme.palette.background.paper,
//     marginTop: theme.spacing(1)
//   },
//   nested: {
//     paddingLeft: theme.spacing(4)
//   }
// }));

// const LinkRouter = props => <Link {...props} component={RouterLink} />;

// export default function RouterBreadcrumbs() {
//   const classes = useStyles();
//   const [open, setOpen] = React.useState(true);

//   const handleClick = () => {
//     setOpen(prevOpen => !prevOpen);
//   };

//   return (
//     <MemoryRouter initialEntries={["/inbox"]} initialIndex={0}>
//       <div className={classes.root}>
//         <Route>
//           {({ location }) => {
//             const pathnames = location.pathname.split("/").filter(x => x);

//             return (
//               <Breadcrumbs aria-label="breadcrumb">
//                 <LinkRouter color="inherit" to="/">
//                   Home
//                 </LinkRouter>
//                 {pathnames.map((value, index) => {
//                   const last = index === pathnames.length - 1;
//                   const to = `/${pathnames.slice(0, index + 1).join("/")}`;

//                   return last ? (
//                     <Typography color="textPrimary" key={to}>
//                       {breadcrumbNameMap[to]}
//                     </Typography>
//                   ) : (
//                     <LinkRouter color="inherit" to={to} key={to}>
//                       {breadcrumbNameMap[to]}
//                     </LinkRouter>
//                   );
//                 })}
//               </Breadcrumbs>
//             );
//           }}
//         </Route>
//         <nav className={classes.lists} aria-label="mailbox folders">
//           <List>
//             <ListItemLink to="/inbox" open={open} onClick={handleClick} />
//             <Collapse component="li" in={open} timeout="auto" unmountOnExit>
//               <List disablePadding>
//                 <ListItemLink
//                   to="/inbox/important"
//                   className={classes.nested}
//                 />
//               </List>
//             </Collapse>
//             <ListItemLink to="/trash" />
//             <ListItemLink to="/spam" />
//           </List>
//         </nav>
//       </div>
//     </MemoryRouter>
//   );
// }

// import React from "react";
// import MaterialTable from "material-table";

// export default function MaterialTableDemo() {
//   const [state, setState] = React.useState({
//     columns: [
//       { title: "Name", field: "name" },
//       { title: "Surname", field: "surname" },
//       { title: "Birth Year", field: "birthYear", type: "numeric" },
//       {
//         title: "Birth Place",
//         field: "birthCity",
//         lookup: { 34: "İstanbul", 63: "Şanlıurfa" }
//       }
//     ],
//     data: [
//       { name: "Mehmet", surname: "Baran", birthYear: 1987, birthCity: 63 },
//       {
//         name: "Zerya Betül",
//         surname: "Baran",
//         birthYear: 2017,
//         birthCity: 34
//       }
//     ]
//   });

//   return (
//     <MaterialTable
//       title="Editable Example"
//       columns={state.columns}
//       data={state.data}
//       editable={{
//         onRowAdd: "+",
//         // onRowAdd: newData =>
//         //   new Promise(resolve => {
//         //     setTimeout(() => {
//         //       resolve();
//         //       setState(prevState => {
//         //         const data = [...prevState.data];
//         //         data.push(newData);
//         //         return { ...prevState, data };
//         //       });
//         //     }, 600);
//         //   }),
//         onRowUpdate: (newData, oldData) =>
//           new Promise(resolve => {
//             setTimeout(() => {
//               resolve();
//               if (oldData) {
//                 setState(prevState => {
//                   const data = [...prevState.data];
//                   data[data.indexOf(oldData)] = newData;
//                   return { ...prevState, data };
//                 });
//               }
//             }, 600);
//           }),
//         onRowDelete: oldData =>
//           new Promise(resolve => {
//             setTimeout(() => {
//               resolve();
//               setState(prevState => {
//                 const data = [...prevState.data];
//                 data.splice(data.indexOf(oldData), 1);
//                 return { ...prevState, data };
//               });
//             }, 600);
//           })
//       }}
//     />
//   );
// }

import React, { Component } from "react";
import ReactDOM from "react-dom";
import MaterialTable from "material-table";

export default class componentName extends Component {
  render() {
    return (
      <div style={{ maxWidth: "100%" }}>
        <MaterialTable
          columns={[
            { title: "Id", field: "lotId" },
            { title: "Nombre de BI", field: "nbBi" }
            // { title: "Doğum Yılı", field: "birthYear", type: "numeric" },
            // {
            //   title: "Doğum Yeri",
            //   field: "birthCity",
            //   lookup: { 34: "İstanbul", 63: "Şanlıurfa" }
            // }
          ]}
          data={[
            {
              lotId: "Mehmet",
              nbBi: "Baran"
              //   birthYear: 1987,
              //   birthCity: 63
            }
          ]}
          actions={[
            {
              icon: "add",
              tooltip: "Récupérer un nouveau lot",
              isFreeAction: true,
              onClick: event => alert("Récupération d'un lot en cours...")
            }
          ]}
          title={"Liste de mes lots"}
          options={{
            paging: false,
            search: false
            // exportAllData: true,
            // exportButton: true
          }}
          //   onRowClick={(event, rowData, togglePanel) => togglePanel()}
        />
      </div>
    );
  }
}
