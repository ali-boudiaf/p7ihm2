import React from "react";
import MesLots from "./meslots";
import Typography from "@material-ui/core/Typography";
import Test from "./Test";
import MaterialTable from "material-table";
import MesLots2 from "./meslotscopy";

const LandingPage = () => (
  <div>
    <Typography variant="h3" component="h2" gutterBottom>
      Mes lots
    </Typography>
    {/* <MesLots /> */}
    <MesLots2 />
    {/* <Test /> */}
  </div>
);
export default LandingPage;
