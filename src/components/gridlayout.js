import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Main from "./main";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    padding: "1em"
  }
}));

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Main />
      {/* <Grid container>
        <Grid item lg={12}>
        </Grid>
      </Grid> */}
    </div>
  );
}
