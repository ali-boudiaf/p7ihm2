import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Grid from "@material-ui/core/Grid";

const END_POINT = "http://localhost:9090/";

// Pour récupérer les lots affecter selon l'idep
const LOTS_REPRISE_Q = "lots-reprise-q/idep/";

// Pour ajouter un nouveau lot à sa liste de lots
const LOTS_REPRISE_Q_PRENDRE = "lots-reprise-q/nonaffecte/";

const MesLots = props => {
  const [lots, setLots] = useState([]);
  const { idep } = props;

  //   Récupération des lots à chaque chargement du composant
  useEffect(() => {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q}${idep}`)
      .then(response => {
        setLots(response.data);
      })
      .catch(error => {});
  }, []);

  const ajoutLot = () => {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q_PRENDRE}${idep}`)
      .then(response => {
        console.log("-------------");
        console.log("ajouter un lot", response);
        console.log("-------------");
        const nouveauLots = [...lots, response.data];
        setLots(nouveauLots);
      })
      .catch(error => {});
  };

  const useStyles = makeStyles(theme => ({
    root: {
      width: "100%",
      overflowX: "auto"
    },
    table: {
      minWidth: 650
    },
    absolute: {
      position: "static",
      bottom: theme.spacing(50),
      right: theme.spacing(0),
      left: theme.spacing(0)
    },
    paper: {
      color: theme.palette.text.secondary
    }
  }));

  const classes = useStyles();

  console.log(lots);

  return (
    <Grid container xs={6}>
      <Grid item xs={12}>
        <Paper className={classes.root}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell align="right">Statut</TableCell>
                <TableCell align="right">Nombre Individus</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {lots.map(lot => (
                <TableRow key={lot.lotId}>
                  <TableCell component="th" scope="row">
                    <Link to={`lot/${lot.lotId}`}>Lot d'id n°{lot.lotId}</Link>
                  </TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right">
                    {lot.individusQualite.length} individus qualité
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
        <Tooltip title="Add" aria-label="add">
          <Fab color="secondary" className={classes.absolute}>
            <AddIcon />
          </Fab>
        </Tooltip>
        {/* <Grid item xs={1}></Grid> */}
      </Grid>
    </Grid>
  );
};

export default MesLots;
