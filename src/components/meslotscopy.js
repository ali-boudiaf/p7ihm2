import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useHistory } from "react-router-dom";
import { SnackbarProvider, useSnackbar } from "notistack";

import MaterialTable from "material-table";

const END_POINT = "http://localhost:9090/";

// Pour récupérer les lots affecter selon l'idep
const LOTS_REPRISE_Q = "lots-reprise-q/idep/";

// Pour ajouter un nouveau lot à sa liste de lots
const LOTS_REPRISE_Q_PRENDRE = "lots-reprise-q/nonaffecte/";

const MesLots2 = props => {
  const [lots, setLots] = useState([]);
  const { idep } = props;

  const { enqueueSnackbar } = useSnackbar();

  //   Récupération des lots à chaque chargement du composant
  useEffect(() => {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q}${idep}`)
      .then(response => {
        setLots(response.data);
      })
      .catch(error => {});
  }, []);

  const ajoutLot = () => {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q_PRENDRE}${idep}`)
      .then(response => {
        console.log("-------------");
        console.log("ajouter un lot", response);
        console.log("-------------");
        const nouveauLots = [...lots, response.data];
        setLots(nouveauLots);
        enqueueSnackbar("Lot récupéré !", {
          variant: "success"
        });
      })
      .catch(error => {
        enqueueSnackbar("Oups... Impossible de récupérer un nouveau lot.", {
          variant: "error"
        });
      });
  };

  // const redirectionForm = rowData => {
  //   // alert("Redirection en cours vers form...");
  //   console.log("rowData", rowData);
  //   // <Link to={`lot/${lot.lotId}`}/>
  //   props.history.push(`/lot/${rowData.lotId}`, "params");
  // };

  let history = useHistory();
  function handleClick(rowData) {
    history.push(`/lot/${rowData.lotId}`);
  }

  console.log("lots", lots);

  return (
    <div style={{ maxWidth: "100%" }}>
      <MaterialTable
        columns={[
          { title: "Id", field: "lotId" },
          { title: "Nombre de BI", field: "nbBi" }
          // { title: "Doğum Yılı", field: "birthYear", type: "numeric" },
          // {
          //   title: "Doğum Yeri",
          //   field: "birthCity",
          //   lookup: { 34: "İstanbul", 63: "Şanlıurfa" }
          // }
        ]}
        data={lots}
        // data={[
        //   {
        //     lotId: "Mehmet",
        //     nbBi: "Baran"
        //     //   birthYear: 1987,
        //     //   birthCity: 63
        //   }
        // ]}
        actions={[
          {
            icon: "add",
            tooltip: "Récupérer un nouveau lot",
            isFreeAction: true,
            onClick: event => ajoutLot()
            // onClick: event => alert("Récupération d'un lot en cours...")
          }
        ]}
        title={"Liste de mes lots"}
        options={{
          paging: false,
          search: false
          // exportAllData: true,
          // exportButton: true
        }}
        onRowClick={(event, rowData) => handleClick(rowData)}
      />
    </div>
  );
};

export default MesLots2;
