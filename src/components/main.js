import React from "react";
import LandingPage from "./landingpage";
// import LotCourant from "../container/lotcourant";
import LotCourant2 from "../container/locourant2";
// import LotCourant3 from "../container/locourant3";
import { Switch, Route } from "react-router-dom";

const Main = () => (
  <Switch>
    <Route exact path="/" component={LandingPage} />
    <Route path="/lot/:id" component={LotCourant2} />
  </Switch>
);

export default Main;
