import React from "react";
import axios from "axios";
import { Formik, Form, Field, FastField, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Button, Grid } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { TextField } from "formik-material-ui";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import { useSnackbar } from "notistack";

// import Divider from "@material-ui/core/Divider";

// const MyTextInput = ({ label, ...props }) => {
//   const [field, meta] = useField(props);
//   return (
//     <>
//       <label htmlFor={props.id || props.name}>{label}</label>
//       <input className="text-input" {...field} {...props} />
//       {meta.touched && meta.error ? (
//         <div className="error">{meta.error}</div>
//       ) : null}
//     </>
//   );
// };

const IndividusQualiteContent = props => {
  const { unIndividus } = props;
  // console.log("props", props);

  // console.log("unIndividus", unIndividus);

  const { enqueueSnackbar } = useSnackbar();

  const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary
    },
    // container: {
    //   display: "flex",
    //   flexWrap: "wrap"
    // },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200
    },
    button: {
      margin: theme.spacing(1)
    },
    appBar: {
      top: "auto",
      bottom: 0
    }
  }));

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <form>
        <Formik
          initialValues={{
            ...unIndividus
          }}
          validationSchema={Yup.object({
            actetX: Yup.string()
              .max(50, "Must be 15 characters or less")
              .required("Required"),
            nomvoiMq: Yup.string()
              .max(20, "Must be 20 characters or less")
              .required("Required")
              .nullable()
          })}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(false);
            axios
              .put("http://localhost:9090/maj-individus-qualite", values)
              .then(function(response) {
                // console.log("Affichage de la requete axios", response);
                enqueueSnackbar("Enregistré !", {
                  variant: "success"
                });
              })
              .catch(function(error) {
                // console.log("Affichage de la requete axios", error);
                enqueueSnackbar(
                  "Oups... L'enregistrement n'a pas eu pu se faire.",
                  {
                    variant: "error"
                  }
                );
              });
          }}
        >
          <Form>
            {/* <AppBar
              position="sticky"
              color="primary"
              className={classes.appBar}
            > */}

            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
            >
              Enregistrer
            </Button>

            <Button
              type="reset"
              variant="contained"
              color="secondary"
              className={classes.button}
            >
              Reset
            </Button>

            <Grid container spacing={1}>
              <Grid item xs={4}>
                <Paper className={classes.paper}>
                  <Typography gutterBottom variant="subtitle1">
                    Variables en X
                  </Typography>
                  <FastField
                    name="actetX"
                    type="text"
                    label="actetX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="actetX" />

                  <FastField
                    name="complX"
                    type="text"
                    label="complX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="complX" />

                  <FastField
                    name="edpX"
                    type="text"
                    label="edpX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="edpX" />

                  <FastField
                    name="sexeX"
                    type="text"
                    label="sexeX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="sexeX" />

                  <FastField
                    name="jnaiX"
                    type="text"
                    label="jnaiX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="jnaiX" />

                  <FastField
                    name="mnaiX"
                    type="text"
                    label="mnaiX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="mnaiX" />

                  <FastField
                    name="anaiX"
                    type="text"
                    label="anaiX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="anaiX" />

                  <FastField
                    name="diplX"
                    type="text"
                    label="diplX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="diplX" />

                  <FastField
                    name="pltX"
                    type="text"
                    label="pltX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="pltX" />

                  <FastField
                    name="dltX"
                    type="text"
                    label="dltX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="dltX" />

                  <FastField
                    name="cltX"
                    type="text"
                    label="cltX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="cltX" />

                  <FastField
                    name="iltX"
                    type="text"
                    label="iltX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iltX" />

                  <FastField
                    name="vardompartX"
                    type="text"
                    label="vardompartX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="vardompartX" />

                  <FastField
                    name="rsX"
                    type="text"
                    label="rsX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="rsX" />

                  <FastField
                    name="typevoiX"
                    type="text"
                    label="typevoiX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="typevoiX" />

                  <FastField
                    name="numvoiX"
                    type="text"
                    label="numvoiX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="numvoiX" />

                  <FastField
                    name="bisterX"
                    type="text"
                    label="bisterX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="bisterX" />

                  <FastField
                    name="nomvoiX"
                    type="text"
                    label="nomvoiX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="nomvoiX" />

                  <FastField
                    name="situatX"
                    type="text"
                    label="situatX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="situatX" />

                  <FastField
                    name="statavX"
                    type="text"
                    label="statavX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="statavX" />

                  <FastField
                    name="tpX"
                    type="text"
                    label="tpX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="tpX" />

                  <FastField
                    name="statX"
                    type="text"
                    label="statX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="statX" />

                  <FastField
                    name="nbsalX"
                    type="text"
                    label="nbsalX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="nbsalX" />

                  <FastField
                    name="emplX"
                    type="text"
                    label="emplX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="emplX" />

                  <FastField
                    name="pospX"
                    type="text"
                    label="pospX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="pospX" />

                  <FastField
                    name="foncX"
                    type="text"
                    label="foncX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="foncX" />

                  <FastField
                    name="profaX"
                    type="text"
                    label="profaX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profaX" />

                  <FastField
                    name="profiX"
                    type="text"
                    label="profiX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profiX" />

                  <FastField
                    name="profsX"
                    type="text"
                    label="profsX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profsX" />

                  <FastField
                    name="cpladrX"
                    type="text"
                    label="cpladrX"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="cpladrX" />
                </Paper>
              </Grid>
              <Grid item xs={4}>
                <Paper className={classes.paper}>
                  <Typography gutterBottom variant="subtitle1">
                    Variables en Q
                  </Typography>
                  <FastField
                    name="nomvoiMq"
                    type="text"
                    label="nomvoiMq"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="nomvoiMq" />

                  <FastField
                    name="cltMQ"
                    type="text"
                    label="cltMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="cltMQ" />

                  <FastField
                    name="cltCMQ"
                    type="text"
                    label="cltCMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="cltCMQ" />

                  <FastField
                    name="actetCMQ"
                    type="text"
                    label="actetCMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="actetCMQ" />

                  <FastField
                    name="actetLMQ"
                    type="text"
                    label="actetLMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="actetLMQ" />

                  <FastField
                    name="inrsMcaQ"
                    type="text"
                    label="inrsMcaQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="inrsMcaQ" />

                  <FastField
                    name="noteMcaNomQ"
                    type="text"
                    label="noteMcaNomQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="noteMcaNomQ" />

                  <FastField
                    name="noteMcaAdrQ"
                    type="text"
                    label="noteMcaAdrQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="noteMcaAdrQ" />

                  <FastField
                    name="classeQ"
                    type="text"
                    label="classeQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="classeQ" />

                  <FastField
                    name="iMcaQ"
                    type="text"
                    label="iMcaQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iMcaQ" />

                  <FastField
                    name="iRepriseActQ"
                    type="text"
                    label="iRepriseActQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iRepriseActQ" />

                  <FastField
                    name="profaLMQ"
                    type="text"
                    label="profaLMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profaLMQ" />

                  <FastField
                    name="profaCMQ"
                    type="text"
                    label="profaCMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profaCMQ" />

                  <FastField
                    name="profiLMQ"
                    type="text"
                    label="profiLMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profiLMQ" />

                  <FastField
                    name="profiCMQ"
                    type="text"
                    label="profiCMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profiCMQ" />

                  <FastField
                    name="profsLMQ"
                    type="text"
                    label="profsLMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profsLMQ" />

                  <FastField
                    name="profsCMQ"
                    type="text"
                    label="profsCMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="profsCMQ" />

                  <FastField
                    name="iCltMQ"
                    type="text"
                    label="iCltMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iCltMQ" />

                  <FastField
                    name="dltCMQ"
                    type="text"
                    label="dltCMQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="dltCMQ" />

                  <FastField
                    name="iActetQ"
                    type="text"
                    label="iActetQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iActetQ" />

                  <FastField
                    name="iSiretQ"
                    type="text"
                    label="iSiretQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iSiretQ" />

                  <FastField
                    name="iProfsQ"
                    type="text"
                    label="iProfsQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iProfsQ" />

                  <FastField
                    name="iProfiQ"
                    type="text"
                    label="iProfiQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iProfiQ" />

                  <FastField
                    name="iProfaQ"
                    type="text"
                    label="iProfaQ"
                    placeholder="Jane"
                    component={TextField}
                    className={classes.textField}
                  />
                  <ErrorMessage name="iProfaQ" />
                </Paper>
              </Grid>
              <Grid item xs={4}>
                <Paper className={classes.paper}>
                  <Typography gutterBottom variant="subtitle1">
                    Aide
                  </Typography>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                    rhoncus a massa quis rutrum. Aliquam convallis ac augue sit
                    amet ullamcorper. Sed pretium nulla non turpis lobortis
                    tristique. Donec blandit tincidunt ante eu feugiat. Duis
                    convallis purus augue, sit amet facilisis velit cursus ut.
                    Integer aliquet massa non molestie ultrices. Donec cursus
                    libero eu neque congue, sit amet tristique quam tincidunt.
                    Ut risus dui, volutpat vel ante nec, dapibus elementum
                    libero. Phasellus vel interdum metus. Nulla maximus, metus
                    in tristique tempor, magna sapien consequat risus, eu
                    imperdiet enim ante eget odio. Sed nisi velit, tincidunt in
                    quam a, commodo ullamcorper velit. Duis elementum ante
                    risus, sit amet consequat sapien fermentum et. Mauris
                    viverra odio ut ex vestibulum, sed sollicitudin lorem
                    convallis. Nulla sollicitudin consectetur dictum. Donec nec
                    quam non est condimentum mattis. Integer iaculis turpis a
                    ornare consectetur. Duis vitae condimentum nibh, id iaculis
                    ex. Morbi sollicitudin erat vel mauris laoreet sodales.
                    Donec lorem urna, porta non suscipit a, feugiat sit amet
                    dui. Aliquam leo sapien, sagittis sed malesuada ut,
                    consequat at risus. Nulla facilisi. Pellentesque habitant
                    morbi tristique senectus et netus et malesuada fames ac
                    turpis egestas.
                  </Typography>
                </Paper>
              </Grid>
            </Grid>

            {/* <button type="submit">Submit</button> */}

            {/* <button type="reset">Reset</button> */}
            {/* <pre>{JSON.stringify(props, null, 2)}</pre> */}
          </Form>
        </Formik>
      </form>
    </div>
  );
};

export default IndividusQualiteContent;
