import { combineReducers } from "redux";
import lotCourantReducer from "./reducer_lot_courant";

// export default combineReducers({

// });

const rootReducer = combineReducers({
  lotCourantReducer
});

export default rootReducer;
