import React, { useState, useEffect } from "react";
import axios from "axios";

import { connect } from "react-redux";
import IndividusQualiteContent from "../components/individusqualitecontent";

import SwipeableViews from "react-swipeable-views";
import { bindKeyboard } from "react-swipeable-views-utils";
import Typography from "@material-ui/core/Typography";

import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

// const BindKeyboardSwipeableViews = bindKeyboard(SwipeableViews);

const styles = {
  tabs: {
    background: "#fff"
  },
  slide: {
    padding: 15,
    minHeight: 100,
    color: "#fff"
  },
  slide1: {
    backgroundColor: "#FEA900"
  },
  slide2: {
    backgroundColor: "#B3DC4A"
  },
  slide3: {
    backgroundColor: "#6AC0FF"
  }
};

const END_POINT = "http://localhost:9090/";
const LOTS_REPRISE_Q_BY_ID = "lots-reprise-q/id/";

const LotCourant = props => {
  const [lotCourant, setLotCourant] = useState([]);
  const [index, setIndex] = useState([]);

  const listeDesIndividusQ = lotCourant.individusQualite;
  console.log("liste des individusQ: ", listeDesIndividusQ);

  let listItems;
  let nbIndividusQ;
  console.log("nb individusQ", nbIndividusQ);

  const { lotStore } = props;
  const idLotCourant = props.match.params.id;

  const handleChange = (event, value) => setIndex(value);
  const handleChangeIndex = index => setIndex(index);

  useEffect(() => {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q_BY_ID}${idLotCourant}`)
      .then(response => {
        console.log(
          "résultat de la requête affichage du lot courant",
          response
        );
        setLotCourant(response.data);
      })
      .catch(error => {});
  }, []);

  if (listeDesIndividusQ) {
    nbIndividusQ = lotCourant.individusQualite.length;

    listItems = listeDesIndividusQ.map((unIndividus, index) => (
      <div key={index}>
        <IndividusQualiteContent
          unIndividus={unIndividus}
          numeroIndividus={index + 1}
          totalIndividus={nbIndividusQ}
        />
      </div>
    ));
  } else {
    listItems = "test";
  }
  console.log("listItems", listItems);

  return (
    <>
      <div>
        <Typography variant="h3" component="h2" gutterBottom>
          Affichage d'un lot avec ses individus
        </Typography>

        <p>Id du lot selectionné par l'utilisateur : {lotStore.idLot}</p>
        <p>Le lot contient {nbIndividusQ}</p>
        <p>ID dans l'url = {props.match.params.id}</p>
        <p>ID dans l'url = {idLotCourant}</p>
        {/* <ul>{listItems}</ul> */}
      </div>

      <div>
        <Tabs
          value={index}
          fullWidth
          onChange={handleChange}
          style={styles.tabs}
        >
          <Tab label="tab n°1" />
          <Tab label="tab n°2" />
          <Tab label="tab n°3" />
        </Tabs>
        <SwipeableViews index={index} onChangeIndex={handleChangeIndex}>
          <div style={Object.assign({}, styles.slide, styles.slide1)}>
            slide n°1
          </div>
          <div style={Object.assign({}, styles.slide, styles.slide2)}>
            slide n°2
            <Select value={10} autoWidth={false}>
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Ten</MenuItem>
            </Select>
          </div>
          <div style={Object.assign({}, styles.slide, styles.slide3)}>
            slide n°3
          </div>
        </SwipeableViews>
      </div>

      {/* <BindKeyboardSwipeableViews> */}
      {/* <div style={Object.assign({}, styles.slide, styles.slide1)}>
          slide n°1
        </div>
        <div style={Object.assign({}, styles.slide, styles.slide2)}>
          slide n°2
        </div>
        <div style={Object.assign({}, styles.slide, styles.slide3)}>
          slide n°3
        </div> */}
      {/* {listItems}
      </BindKeyboardSwipeableViews> */}
    </>
  );
};

const mapStateToProps = state => {
  return {
    lotStore: state.lotCourantReducer
  };
};
export default connect(mapStateToProps)(LotCourant);
