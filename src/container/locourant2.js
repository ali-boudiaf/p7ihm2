import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import axios from "axios";
// import Container from "@material-ui/core/Container";
import LinearProgress from "@material-ui/core/LinearProgress";

import IndividusQualiteContent from "../components/individusqualitecontent";
// import { Paper } from "@material-ui/core";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export default function FullWidthTabs(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = useState(0);
  const [lotCourant, setLotCourant] = useState([]);

  const listeDesIndividusQ = lotCourant.individusQualite;
  // console.log("liste des individusQ: ", listeDesIndividusQ);
  let listItems;
  let listItemsTabHead;
  let nbIndividusQ;
  // console.log("nb individusQ", nbIndividusQ);
  const idLotCourant = props.match.params.id;
  // console.log("idLotCourant :", idLotCourant);
  // console.log("props: ", props);
  const END_POINT = "http://localhost:9090/";
  const LOTS_REPRISE_Q_BY_ID = "lots-reprise-q/id/";

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = index => {
    setValue(index);
  };

  useEffect(() => {
    axios
      .get(`${END_POINT}${LOTS_REPRISE_Q_BY_ID}${idLotCourant}`)
      .then(response => {
        console.log(
          "résultat de la requête affichage du lot courant",
          response
        );
        setLotCourant(response.data);
      })
      .catch(error => {});
  }, []);

  if (listeDesIndividusQ) {
    nbIndividusQ = lotCourant.individusQualite.length;

    listItems = listeDesIndividusQ.map((unIndividus, index) => (
      <TabPanel key={index} value={value} index={index} dir={theme.direction}>
        <IndividusQualiteContent
          unIndividus={unIndividus}
          numeroIndividus={index + 1}
          totalIndividus={nbIndividusQ}
        />
      </TabPanel>
    ));
    listItemsTabHead = listeDesIndividusQ.map((unIndividus, index) => (
      <Tab key={index} label={unIndividus.actetX} {...a11yProps(0)} />
    ));
  } else {
    listItems = <LinearProgress color="secondary" />;
  }
  console.log("listItems", listItems);
  console.log("listItemsTabHead", listItemsTabHead);

  return (
    <div className={classes.root}>
      <AppBar position="sticky" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="on"
          aria-label="full width tabs example"
        >
          {listItemsTabHead}
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        {listItems}
      </SwipeableViews>
    </div>
  );
}
